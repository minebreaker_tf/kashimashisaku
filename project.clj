(defproject kashima_shisaku "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :plugins [[lein-ring "0.9.7"]
            [lein-cljsbuild "1.1.6"]]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.521"]
                 ;[org.clojure/java.jdbc "0.7.0-alpha3"]
                 [compojure "1.6.0"]
                 [ring/ring-defaults "0.2.1"]
                 ;[com.h2database/h2 "1.4.195"]
                 ;[com.google.guava/guava "22.0"]
                 [org.jetbrains.xodus/xodus-environment "1.0.5"]
                 [org.jetbrains.xodus/xodus-entity-store "1.0.5"]
                 [cheshire "5.7.1"]
                 [org.yaml/snakeyaml "1.18"]]
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}}
  :ring {:handler kashima-shisaku.handler/app}
  :source-paths ["src/clj"]
  :cljsbuild {
              :builds [{:id           "kashima_shisaku"
                        :source-paths ["src/cljs"]
                        :compiler     {:output-to    "resources/public/js/cljs.js"
                                       :output-dir   "resources/public/js"
                                       :pretty-print true}}]})
