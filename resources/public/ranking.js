'use strict';

const load = function (that) {
    fetch('/api/ranking?n=20&offset=0')
        .then(function (r) {
            if (r.ok) {
                return r.json();
            } else {
                console.warn("Failed to load");
                return null;
            }
        })
        .then(function (r) {
            if (r) {
                that.images = r;
            }
        });
};

const images = {
    name: "images",
    template: "" +
    "<div>" +
    "   <img class='ranking' v-for='i in images' :src=\"'/api/resource/' + i.id\">" +
    "</div>",
    data: function () {
        return {
            images: []
        }
    },
    beforeCreate: function () {
        load(this);
    }
};

Vue.component('root', {
    template: '<images></images>',
    components: {
        images: images
    }
});

new Vue({
    el: 'root'
});
