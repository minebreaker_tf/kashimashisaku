'use strict';

const loadNext = function (that) {
    fetch("/api/next")
        .then(function (r) {
            if (r.ok) {
                return r.json();
            } else {
                console.log("called3");
                console.warn("Failed to load");
                return null;
            }
        })
        .then(function (r) {
            if (r) {
                that.left = r[0].id;
                that.right = r[1].id;
                that.show = true;
            }
        });
};

const vote = function (up, down, callback) {
    fetch("/api/update?" + "up=" + up + "&down=" + down, { method: 'POST' })
        .then(function (r) {
            if (r.ok) {
                return r.json();
            } else {
                console.warn("Failed to vote");
                return null;
            }
        })
        .then(function (r) {
            if (r && callback) {
                callback(r);
            }
        });
};

const imageCase = {
    name: "image-case",
    template: "" +
    "<div>" +
    "   <img class='update' v-if='show' :src=\"'/api/resource/' + left\" v-on:click='voteLeft'>" +
    "   <img class='update' v-if='show' :src=\"'/api/resource/' + right\" v-on:click='voteRight'>" +
    "</div>",
    data: function () {
        return {
            show: false,
            left: -1,
            right: -1
        }
    },
    beforeCreate: function () {
        loadNext(this);
    },
    methods: {
        voteLeft: function () {
            this.show = false;
            const that = this;
            vote(this.left, this.right, function () {
                loadNext(that);
            });
        },
        voteRight: function () {
            this.show = false;
            const that = this;
            vote(this.right, this.left, function () {
                loadNext(that);
            });
        }
    }
};

Vue.component('root', {
    template: '<image-case></image-case>',
    components: {
        imageCase: imageCase
    }
});

new Vue({
    el: 'root'
});
