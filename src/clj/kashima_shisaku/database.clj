(ns kashima-shisaku.database
  (:require [kashima-shisaku.config :as conf]
            [cheshire.core :as cheshire])
  (:import (jetbrains.exodus.entitystore PersistentEntityStore
                                         PersistentEntityStores
                                         PersistentEntityId
                                         StoreTransactionalComputable
                                         EntityRemovedInDatabaseException Entity StoreTransaction)
           (jetbrains.exodus.env Transaction)))

(def store
  "データベース"
  (PersistentEntityStores/newInstance ^String conf/database))

(defn stc
  "computeInTransaction用のインターフェースを、引数の関数で実装したクラスを返す."
  [lambda] (reify StoreTransactionalComputable
             (compute [this txn] (lambda txn))))

(defmacro in-transaction
  []
  )

(defn add-entry
  "エントリーを追加する."
  [type entry-map]
  (.computeInTransaction store
    (stc (fn [txn]
           (let [entry (.newEntity txn type)]
             (doseq [[k v] entry-map]
               (if-not (= k :id)
                 (.setProperty entry (name k) v)))))))
  true)

(defn- entity-to-map
  "jetbrains.exodus.entitystore.Entityをマップに変換する."
  [^Entity e]
  {:id    (-> (.getId e) (.getLocalId))
   :path  (.getProperty e "path")
   :eval  (.getProperty e "eval")
   :times (.getProperty e "times")})

(defn update-entity
  "エンティティを更新する."
  [type-id id updater]
  (.computeInTransaction store
    (stc (fn [txn]
           (let [entity (.getEntity txn (PersistentEntityId. type-id id))
                 updated-entity (updater (entity-to-map entity))]
             (doseq [[k v] updated-entity]
               (if-not (= k :id)
                 (.setProperty entity (name k) v))))))))

(defn find-entry
  "指定IDのエンティティを返す. 見つからなかった場合nilを返す."
  [type-id local-id]
  (.computeInTransaction store
    (stc (fn [txn]
           (try
             (let [e (.getEntity txn (PersistentEntityId. type-id local-id))]
               (entity-to-map e))
             (catch EntityRemovedInDatabaseException _ nil))))))

(defn get-entity-type-id
  "エンティティ名からエンティティタイプIDを取得する."
  [entity-name]
  (.computeInTransaction store
    (stc (fn [txn] (.getEntityTypeId store entity-name)))))

(defn get-all-entities
  [entity-name]
  (.computeInTransaction store
    (stc (fn [^StoreTransaction txn]
           (doall (map entity-to-map (.getAll txn entity-name)))))))

(defn clear [] (.clear store))
(defn close [] (.close store))

(comment
  "エンティティの構造"

  "User"
  {:id    "Local ID"
   :path  "ファイルのパス @NaturalId"
   :eval  "評価数値 大きいほど高評価"
   :times "そのファイルが評価された回数"}
  )
