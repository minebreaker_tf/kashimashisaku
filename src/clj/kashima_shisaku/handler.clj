(ns kashima-shisaku.handler
  (:require [kashima-shisaku.indexing :as ind]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.util.response :as resp]
            [cheshire.core :as cheshire]))

(defn reindex
  []
  (cheshire/generate-string
    (try
      (ind/index-all-files)
      {:result "success"}
      (catch Exception e
        {:result "failure" :reason e}))))

(defn get-ranking
  [param]
  (cheshire/generate-string
    (ind/get-top-rate (Integer/parseInt (param :offset)) (Integer/parseInt (param :n)))))

(defn get-next-eval
  []
  (cheshire/generate-string
    (ind/next-eval)))

(defn update-eval
  [param]
  (cheshire/generate-string
    (if (ind/eval-index (param :up) (param :down))
      {:result "success"}
      {:result "failure"})))

(defn get-file
  [id]
  (let [f (ind/get-file id)
        get-ext (fn -get-ext
                  [name]
                  (.substring name (inc (.lastIndexOf name ".")) (.length name)))]
    (assoc
      (resp/file-response f)
      :headers
      {"Content-Type" (str "image/" (get-ext f))})))

(defn get-file-info
  [id]
  (cheshire/generate-string
    (ind/get-file-info id)))

(defroutes app-routes
  (GET "/" [] (resp/redirect "index.html"))
  (GET "/api/reindex" [] (reindex))
  (GET "/api/ranking" {param :params} (get-ranking param))
  (GET "/api/next" [] (get-next-eval))
  (POST "/api/update" {param :params} (update-eval param))
  (GET "/api/resource/:id" [id] (get-file id))
  (GET "/api/resource/:id/info" [id] (get-file-info id))
  (DELETE "/api/clear" [] (do (kashima-shisaku.database/clear) "ok"))
  (route/not-found "Not Found"))

(def app (wrap-defaults app-routes (assoc-in site-defaults [:security :anti-forgery] false)))
