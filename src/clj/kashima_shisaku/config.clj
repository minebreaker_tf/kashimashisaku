(ns kashima-shisaku.config
  (:import (org.yaml.snakeyaml Yaml)))

(def document
  "コンフィグファイルのパス"
  "conf.yaml")

(defn- load-config
  "コンフィグをロードする."
  []
  (.load (new Yaml) (.getResourceAsStream (ClassLoader/getSystemClassLoader) document)))

(def conf
  "コンフィグオブジェクト"
  (load-config))

;(def jdbc {:url (get (get conf "jdbc") "url")})

(def files
  "画像ファイルのルートディレクトリ"
  (get conf "files"))
(def database
  "データベースのルートディレクトリ"
  (get conf "database"))
