(ns kashima-shisaku.indexing
  (:require [clojure.java.io :as io]
            [kashima-shisaku.config :as conf]
            [kashima-shisaku.database :as db])
  (:import (java.io File)
           (com.google.common.io ByteStreams)))

(def entity-type-file
  "ファイル・エンティティタイプ"
  "File")

(def entity-type-id-file
  "ファイル・エンティティタイプのID"
  (db/get-entity-type-id entity-type-file))

(defn index-all-files
  "全ファイルのインデックスを(再)作成する"
  []
  (let [files (->> (file-seq (clojure.java.io/file conf/files))
                (filter #(.isFile %))
                (map #(.getCanonicalPath %)))]
    (doseq [f files]
      (comment (if エンティティが既に存在したら) 更新すべきかチェックして更新)
      (db/add-entry entity-type-file {"path" f "eval" 0 "times" 0}))))

(defn get-file-info
  "ファイルの情報を返却する. 見つからなければnilを返す."
  [id]
  (db/find-entry entity-type-id-file (Integer/parseInt id)))

(defn get-file
  "ファイルのパスを返す. 見つからなければnilを返す."
  [id]
  (if-let [e (get-file-info id)]
    (e :path)))

(defn compare-eval
  "評価順コンパレーター"
  [e1 e2]
  (let [t1 (e1 :times)
        e1 (e1 :eval)
        t2 (e2 :times)
        e2 (e2 :eval)]
    (cond
      (or (zero? t1) (zero? t2)) (compare e2 e1)
      :else (compare
              (/ e2 t2) (/ e1 t1)))))

(defn get-top-rate
  "ファイルの評価の高い順に、IDのベクターを返す."
  [offset num]
  (->> (db/get-all-entities entity-type-file)
    (sort compare-eval)
    (drop offset)
    (take num)
    (map-indexed (fn [i e] (dissoc (assoc e :rank i) :path)))
    (vec)))

(defn next-eval
  "次に評価すべき2つのファイルのIDを返す.
  評価された回数が少ないものの中から、評価順が最も低い2つを返す"
  []
  (->> (db/get-all-entities entity-type-file)
    (shuffle)
    (sort compare-eval)
    (sort #(compare (%1 :times) (%2 :times)))
    (take 2)
    (map-indexed (fn [i e] (dissoc (assoc e :rank i) :path)))
    (vec)))

(defn eval-index
  "2つのファイルを比較し、それぞれの評価を向上、下落させる."
  [up-id down-id]
  (do
    (db/update-entity
      entity-type-id-file
      (Integer/parseInt up-id)
      #(update (update %1 :eval inc) :times inc))
    (db/update-entity
      entity-type-id-file
      (Integer/parseInt down-id)
      #(update %1 :times inc))
    true))
