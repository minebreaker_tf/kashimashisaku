(ns kashima-shisaku.update)

(enable-console-print!)

(defn ajax
  [url callback]
  (let [req (js/fetch url)]
    (.then req
      (fn [res]
        (if (.-ok res)
          (.json res)
          (print "error!"))))))

(defn load-next
  [this]
  (ajax "/api/next"
    #(let [res %1
           left (first res)
           right (first (rest res))]
       (set! (.-left this) (js-obj left))
       (set! (.-right this) #js {:id 1}))))

(defn vote
  [up down]
  (ajax
    (str "/api/update?" "up=" (:id up) "&" "down=" (:id down))
    #(let [res %1]
       (println res))))

(def image-case
  #js {:name        "image"
       :template    "<div>
                      {{ left }}<br/>
                      {{ right }}<br/>
                      <img :src=\"'/api/resource/' + left.id\" v-on:click=\"vote-right\">
                      <img :src=\"'/api/resource/' + right.id\" v-on:click=\"vote-left\">
                    </div>"
       :beforeMount #(this-as this (load-next this))
       ;:beforeMount #(this-as this (set! (.-left this) #js {:id 1}))
       :data        (fn [] #js {:left  {}
                                :right {}})
       :methods     #js {:vote-right #(this-as this (do
                                                      (vote (this :left) (this :right))
                                                      (load-next this)))
                         :vote-left  #(this-as this (do
                                                      (vote (this :right) (this :left))
                                                      (load-next this)))}})

(.component js/Vue "root"
  #js {:template    "<image-case></image-case>"
       :components  #js {:image-case image-case}
       :beforeMount load-next})

; Initialize Vue
(new js/Vue
  #js {:el "root"})
