(ns kashima-shisaku.index)

(enable-console-print!)

(def hello
  #js {:name     "hello"
       :template "<div>
                   <a href=\"update.html\">Sort</a>
                   <a href=\"ranking.html\">Ranking</a>
                 </div>"})

(.component js/Vue "root"
  #js {:template   "<hello></hello>"
       :components #js {:hello hello}})

; Initialize Vue
(new js/Vue
  #js {:el "root"})
